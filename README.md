# cobox-dot-cloud

<div align="center">
  <img src="https://cobox.cloud/src/svg/logo.svg">
</div>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Table of Contents

- [About](#about)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## About
**CoBox** is an encrypted p2p file system and distributed back-up tool. [README](https://gitlab.com/coboxcoop/readme) provides a map of the project.

`cobox-dot-cloud` is a [Kirby](https://getkirby.com/) website for [CoBox](https://cobox.cloud/).

## Install

Run `composer install` and `npm install` to install dependencies. We’re using using [Prettier](https://prettier.io/) and [stylelint](https://stylelint.io/) for code formatting.

If you need to change any config settings for your local environment, add the necessary Kirby config file corresponding with your local environment domain ([see docs](https://getkirby.com/docs/guide/configuration#multi-environment-setup)).

### Activity log

The NPM registry activity log is generated with JavaScript from the `/assets/data.json` file. This is created once an hour on the server via a cron job.

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

GPL-v3
