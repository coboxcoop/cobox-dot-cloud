<?php

function getColor($color) {
  if ($color) {
    return 'theme-' . $color;
  } else {
    return false;
  }
}

return [
  'thumbs' => [
    'presets' => [
      'default' => ['width' => 1024, 'quality' => 80],
      'icon' => ['width' => 160, 'quality' => 70, 'grayscale' => true],
      'feature' => ['width' => 2000, 'quality' => 80],
      'cover' => ['width' => 3000, 'quality' => 80],
      'profile' => ['width' => 1000, 'height' => 1000, 'quality' => 70, 'crop' => true]
    ],
    'srcsets' => [
      'default' => [900, 2000],
      'feature' => [
        '900w' => ['width' => 900, 'quality' => 80],
        '2000w' => ['width' => 2000, 'quality' => 80]
      ],
      'cover' => [
        '900w' => ['width' => 900, 'quality' => 80],
        '1500w' => ['width' => 1500, 'quality' => 80],
        '3000w' => ['width' => 3000, 'quality' => 80]
      ],
      'profile' => [
        '500w' => ['width' => 500, 'height' => 1000, 'quality' => 70, 'crop' => true],
        '1000w' => ['width' => 1000, 'height' => 1000, 'quality' => 70, 'crop' => true]
      ]
    ]
  ]
];