<?php snippet('header') ?>

<article class="page">

  <div class="content">

    <?php snippet('cover') ?>

    <header class="page__header">
      <h1 class="page__title"><?php echo $page->title()->html()->smartypants() ?></h1>
    </header>

    <?php snippet('body') ?>

  </div>

  <?php snippet('blocks') ?>

</article>

<?php snippet('footer') ?>
