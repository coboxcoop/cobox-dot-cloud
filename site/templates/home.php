<?php snippet('header') ?>

<article class="page">
  <div class="content">
    <header class="page__header">
      <div class="logo-row">
        <?= svg('assets/svg/logo.svg') ?>
      </div>
    </header>
  </div>

  <?php snippet('blocks') ?>

  <section id="activity" class="activity activity--hidden">
    <div id="activity-inner" class="activity__inner">
      <?php if ($page->activityheading()->isNotEmpty()) : ?>
        <p class="activity__label"><?= $page->activityheading()->html()->smartypants() ?></p>
      <?php endif ?>
      <ul id="activity-list" class="activity__list">
      </ul>
      <?php $activitylink = $page->activitylink() ?>
      <?php $activitylinktext = $page->activitylinktext() ?>
      <?php if ($activitylink->isNotEmpty() && $activitylinktext->isNotEmpty()) : ?>
        <a href="<?= $activitylink ?>" class="activity__cta"><?= $activitylinktext->html()->smartypants() ?></a>
      <?php endif ?>
    </div>
  </section>

</article>

<?php snippet('footer') ?>
