<?php

return function ($page, $site) {
  $title = $site->title()->smartypants();
  $title = $site->strapline()->isNotEmpty() ? $title . ' &mdash; ' . $site->strapline()->kirby()->smartypants() : $title;
  return [
    'title' => $title
  ];
};