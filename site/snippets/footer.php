  <?php if ($site->footer()) : ?>
    <footer class="site-footer">
      <div class="site-footer__inner">
        <?php $footerItems = $site->footer()->toStructure() ?>
        <?php if ($footerItems->isNotEmpty()) : ?>
          <?php foreach ($footerItems as $item): ?>
            <?= strip_tags($item->text()->kirbytext()->smartypants(), '<p><a><br><strong><italic>') ?>
            <?php $entries = $item->entries() ?>
            <ul class="site-footer__list">
              <?php foreach ($entries->toStructure() as $entry): ?>
                <?php if ($image = $entry->image()->toFile()) : ?>
                  <li class="site-footer__item">
                    <a href="<?= $entry->linkUrl() ?>">
                      <img class="site-footer__image" alt="<?= $entry->entryname() ?>" src="<?= $image->thumb('icon')->url() ?>" />
                    </a>
                  </li>
                <?php endif ?>
              <?php endforeach ?>
          </ul>
          <?php endforeach ?>
        <?php endif ?>
      </div>
    </footer>
  <?php endif ?>

  <?= js('/assets/js/scripts.min.js') ?>

</body>
</html>
