<?php $image = $data->image()->toFile() ?>
<?php $preheading = $data->preheading() ?>
<?php $heading = $data->heading() ?>
<?php $desc = strip_tags($data->desc()->kirbytext()->smartypants(), '<a><p><br><strong><code><em>') ?>
<?php $ctaText = $data->ctatext() ?>
<?php $ctaUrl = $data->ctaUrl() ?>
<?php if ($image->isNotEmpty() && $preheading->isNotEmpty() && $heading->isNotEmpty() && $desc) : ?>
  <?php $caption = $image->caption() ?>
  <section class="feature<?= e($data->reverse()->toBool(), ' feature--reverse') ?><?= e($data->blend()->toBool(), ' feature--blend') ?> <?= getColor($data->color()) ?>">
    <div class="feature__inner">
      <div class="feature__content">
        <p class="feature__label"><?= $preheading ?></p>
        <h2 class="feature__title">
          <?= $heading ?>
        </h2>
        <div class="feature__text">
          <?= $desc ?>
        </div>
        <?php if ($ctaText->isNotEmpty() && $ctaUrl->isNotEmpty()) : ?>
          <a href="<?= $ctaUrl ?>" class="feature__cta"><?= $ctaText ?></a>
        <?php endif ?>
      </div>
      <div class="feature__image">
        <figure>
          <img
            src="<?= $image->thumb('feature')->url() ?>"
            srcset="<?= $image->srcset('feature') ?>"
            sizes="100vw, (min-width: 960px) 53vw, (min-width: 1280px) 690px"
            <?php if ($image->alt()->isNotEmpty()) : ?>alt="<?= $image->alt() ?>"<?php endif ?>
          />
          <?php $caption = $image->caption() ?>
          <?php if ($caption) : ?>
            <figcaption>
              <?= strip_tags($caption->kirbytext()->smartypants(), '<a><em><strong>') ?>
            </figcaption>
          <?php endif ?>
        </figure>
      </div>
    </div>
  </section>
<?php endif ?>