<?php if ($page->groups()->isNotEmpty()) : ?>
  <?php foreach ($page->groups()->toBuilderBlocks() as $block) : ?>
    <section>
      <?php $text = $block->text() ?>
      <?php $heading = $block->heading() ?>
      <?php if ($heading || $text) : ?>
        <div class="content">
          <div class="page__body">
            <?php if ($heading) : ?>
              <h2><?= $heading->html()->smartypants() ?></h2>
            <?php endif ?>
            <?= $block->text()->kirbytext()->smartypants() ?>
          </div>
        </div>
      <?php endif ?>
      <?php if ($block->entries()->isNotEmpty()) : ?>
        <div class="profiles profiles--<?= $block->_key() ?>">
          <?php foreach ($block->entries()->toStructure() as $entry) : ?>
            <?php $name = $entry->entryname() ?>
            <?php $image = $entry->image() ?>
            <?php if ($name->isNotEmpty()) : ?>
              <div class="profile">
                <?php if ($block->_key() == 'orgs') : ?>
                  <?php $link = $entry->url()->isEmpty() ? false : $entry->url() ?>
                  <?php if ($link) : ?>
                    <a href="<?= $link ?>">
                      <?php if ($image->isEmpty()) : ?>
                        <?= $name->smartypants() ?>
                      <?php else : ?>
                        <?php snippet('org-image', ['image' => $image->toFile(), 'alt' => $name]) ?>
                      <?php endif ?>
                    </a>
                  <?php else : ?>
                    <?php if ($image->isEmpty()) : ?>
                      <?= $name->smartypants() ?>
                    <?php else : ?>
                      <?php snippet('profile-image', ['image' => $image->toFile(), 'alt' => $name]) ?>
                    <?php endif ?>
                  <?php endif ?>
                <?php elseif ($block->_key() == 'people') : ?>
                  <?php snippet('profile-image', ['image' => $image->toFile(), 'alt' => $name]) ?>
                  <?php if ($name) : ?>
                    <h3 class="profile__name"><?= $name->smartypants() ?></h3>
                  <?php endif ?>
                  <?php if ($role = $entry->role()) : ?>
                    <p class="profile__role"><?= $role->smartypants() ?></p>
                  <?php endif ?>
                  <?php if ($entry->links()->isNotEmpty()) : ?>
                    <ul class="profile__linklist">
                      <?php foreach ($entry->links()->toStructure() as $link) : ?>
                        <?php $linkUrl = $link->url() ?>
                        <?php $linkText = $link->text() ?>
                        <?php if ($linkUrl && $linkText) : ?>
                          <li class="profile__linklist-item">
                            <a class="profile__link btn btn--external btn--small" href="<?= $linkUrl ?>"><?= $linkText ?></a>
                          </li>
                        <?php endif ?>
                      <?php endforeach ?>
                    </ul>
                  <?php endif ?>
                <?php endif ?>
              </div>
            <?php endif ?>
          <?php endforeach ?>
        </div>
      <?php endif ?>
    </section>
  <?php endforeach ?>
<?php endif ?>
