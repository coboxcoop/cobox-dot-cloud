<?php if ($cover = $page->cover()->toFile()) : ?>
  <figure class="page__cover">
    <img
      class="page__cover-image"
      src="<?= $cover->thumb('cover')->url() ?>"
      srcset="<?= $cover->srcset('cover') ?>"
      sizes="96vw, (min-width: 1280px) 1200px"
      <?php if ($cover->alt()->isNotEmpty()) : ?>alt="<?= $cover->alt() ?>"<?php endif ?>
    />
    <?php $caption = $cover->caption() ?>
    <?php if ($caption) : ?>
      <figcaption class="page__cover-caption">
        <?= strip_tags($caption->kirbytext()->smartypants(), '<a><em><strong>') ?>
      </figcaption>
    <?php endif ?>
  </figure>
<?php endif ?>