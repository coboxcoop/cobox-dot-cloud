<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" cntent="ie=edge">
    <?php $sep = ' – '; ?>
    <title><?= e(!$page->isHomePage(), $page->title()->html() . $sep) ?><?= $site->title() ?><?= e($page->isHomePage() && $site->strapline()->isNotEmpty(), $sep . $site->strapline()->smartypants()) ?></title>
    <?= css('/assets/css/main.css') ?>
  </head>
  <body class="body body--<?= $page->template() ?>">

    <?php if ($site->preheader()->isNotEmpty()) : ?>
      <aside class="announcement theme-purple">
        <p class="announcement__text">
          <?= strip_tags($site->preheader()->kirbytext()->smartypants(), '<a><br><strong><em>') ?>
        </p>
      </aside>
    <?php endif ?>

    <header class="site-header">
      <div class="site-header__logo">
        <a href="<?= $site->url() ?>" aria-label="<?= $site->title() ?>">
          <?= svg('assets/svg/logo-small.svg') ?>
        </a>
      </div>

      <?php $menuItems = $site->menu()->toStructure() ?>
      <?php if ($menuItems->isNotEmpty()) : ?>
        <nav class="site-header__nav">
          <ul class="site-header__nav-list">
            <?php $active = $site->url() == $page->url() ? true : false ?>
            <?php foreach ($menuItems as $item): ?>
              <?php $itemPage = $item->page()->toPage() ?>
              <?php if ($itemPage) : ?>
                <?php $itemText = $item->text()->isEmpty() ? $itemPage->title()->html() : $item->text() ?>
                <?php $active = $itemPage->isActive() || $page->isDescendantOf($itemPage) ? true : false ?>
                <li class="site-header__nav-item<?php e($active, ' active') ?>">
                  <a href="<?= $itemPage->url() ?>"><?= $itemText->smartypants() ?></a>
                </li>
              <?php endif ?>
            <?php endforeach ?>
          </ul>
        </nav>
      </header>
    <?php endif ?>
