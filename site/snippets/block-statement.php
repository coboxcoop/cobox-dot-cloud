<?php if ($text = $data->text()) : ?>
  <section class="statement">
    <p class="statement__text">
      <?= str_replace('co-operative', 'co&#8209;operative', strip_tags($text->smartypants()->widont())) ?>
    </p>
  </section>
<?php endif ?>
