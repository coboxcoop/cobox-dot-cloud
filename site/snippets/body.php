<?php if ($page->text()->isNotEmpty()) : ?>
  <section class="page__body">
    <?= $page->text()->kirbytext()->smartypants() ?>
  </section>
<?php endif ?>