<?php if ($boxes = $data->boxes()->toStructure()) : ?>
  <section class="link-block-row">
    <?php foreach ($boxes as $box) : ?>
      <a class="link-block <?= getColor($box->color()) ?>" href="<?= $box->linkUrl() ?>">
        <?php if ($image = $box->image()->toFile()) : ?>
          <img class="link-block__image" alt="" src="<?= $image->thumb('icon')->url() ?>" />
        <?php endif ?>
        <?php if ($heading = $box->heading()) : ?>
          <p class="link-block__title"><?= $heading->smartypants() ?></p>
        <?php endif ?>
        <?php if ($desc = $box->desc()) : ?>
          <p class="link-block__text">
            <?= strip_tags($desc->kirbytext()->smartypants(), '<strong><em><code>') ?>
          </p>
        <?php endif ?>
      </a>
    <?php endforeach ?>
  </section>
<?php endif ?>