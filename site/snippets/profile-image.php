
<img
  class="profile__image"
  src="<?= $image->thumb('profile')->url() ?>"
  srcset="<?= $image->srcset('profile') ?>"
  sizes="100vw, (min-width: 600px) 50vw, (min-width: 768px) 33.3vw, (min-width: 960px) 25vw, (min-width: 1280px) 266px"
  alt="<?= $alt ?>"
>