<?php if ($blocks = $page->blocks()->toBuilderBlocks()) : ?>
  <?php foreach ($blocks as $block) : ?>
    <?php snippet('block-' . $block->_key(), array('data' => $block)) ?>
  <?php endforeach ?>
<?php endif ?>