var autoprefixer = require("gulp-autoprefixer");
var browserSync = require("browser-sync").create();
var concat = require("gulp-concat");
let cleanCSS = require("gulp-clean-css");
var gulp = require("gulp");
var sass = require("gulp-sass");
var uglify = require("gulp-terser");
var cheerio = require("gulp-cheerio");

gulp.task("browserSync", function() {
  browserSync.init({
    proxy: "cobox-dot-cloud.test",
    ghostMode: false,
    notify: false
  });
});

gulp.task("fonts", function() {
  return gulp
    .src(["./src/fonts/**/*", "!./src/fonts/.gitkeep"])
    .pipe(gulp.dest("./assets/fonts/"));
});

gulp.task("js", function() {
  return gulp
    .src(["./src/js/scripts.js", "./node_modules/moment/min/moment.min.js"])
    .pipe(concat("scripts.min.js"))
    .pipe(uglify())
    .pipe(gulp.dest("./assets/js/"))
    .pipe(
      browserSync.reload({
        stream: true
      })
    );
});

gulp.task("sass", function() {
  return gulp
    .src(["./src/css/**/main.scss"])
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(concat("main.css"))
    .pipe(cleanCSS())
    .pipe(gulp.dest("./assets/css/"))
    .pipe(
      browserSync.reload({
        stream: true
      })
    );
});

gulp.task("svg", function() {
  return gulp
    .src(["./src/svg/**/*.svg"])
    .pipe(
      cheerio(function($, file) {
        var fileName = file.relative;
        var className = fileName.replace(".svg", "");
        $("svg").addClass(className);
        $('[fill!="none"]').removeAttr("fill");
      })
    )
    .pipe(gulp.dest("./assets/svg/"));
});

gulp.task("default", gulp.series(["fonts", "js", "sass", "svg"]));

gulp.task(
  "watch",
  gulp.parallel(["default", "browserSync"], function() {
    gulp.watch("./src/js/**/*.js", gulp.parallel("js"));
    gulp.watch("./src/fonts/**/*", gulp.parallel("fonts"));
    gulp.watch("./src/css/**/*.scss", gulp.parallel("sass"));
    gulp.watch("./src/svg/**/*.svg", gulp.parallel("svg"));
    gulp
      .watch(["site/snippets/**/*", "site/templates/**/*"])
      .on("change", browserSync.reload);
  })
);
