// Get the repos from the JSON once an hour
getActivity();
setInterval(() => {
  console.log("get activity");
  getActivity();
}, 3600000);

function getActivity() {
  const activity = document.querySelector("#activity");
  if (!activity) {
    return;
  }
  fetch("/assets/data.json")
    .then(handleResponse)
    .then(data => {
      // Turn data to array
      let repos = Object.keys(data).map(function(key) {
        return data[key];
      });

      // Sort repos by the publish date of the latest version
      repos = repos.sort((a, b) => {
        const bDate = new Date(b.time[b["dist-tags"].latest]);
        const aDate = new Date(a.time[a["dist-tags"].latest]);
        return bDate - aDate;
      });

      // Get and clear the list
      const list = document.querySelector("#activity-list");
      list.innerHTML = "";

      // For each of the repos
      repos.forEach(repo => {
        // Create an item
        const itemElem = document.createElement("li");
        itemElem.classList.add("activity__item", "repo");

        // Create a path
        const pathElem = document.createElement("span");
        pathElem.classList.add("repo__path");

        // Add the author to the path if available, otherwise add the first maintainer
        let handle = false;
        if (repo.author && repo.author.name) {
          handle = document.createTextNode(repo.author.name);
        } else if (repo.maintainers && repo.maintainers[0].name) {
          handle = document.createTextNode(repo.maintainers[0].name);
        }
        if (handle) {
          const authorElem = document.createElement("span");
          authorElem.classList.add("repo__author");
          authorElem.appendChild(handle);
          pathElem.appendChild(authorElem);
        }

        // Add the repo link to the path
        const linkElem = document.createElement("a");
        const linkText = document.createTextNode(repo._id);
        linkElem.classList.add("repo__link");
        linkElem.href = "https://www.npmjs.com/package/" + repo._id;
        linkElem.append(linkText);
        pathElem.appendChild(linkElem);

        // Add the path to the item
        itemElem.appendChild(pathElem);

        // Create the meta
        const metaElem = document.createElement("span");
        metaElem.classList.add("repo__meta");

        // Get the most recent version and time
        const version = repo["dist-tags"].latest;
        const time = moment(repo.time[version]).fromNow();

        // Add the version
        const versionElem = document.createElement("span");
        const versionText = document.createTextNode(version);
        versionElem.classList.add("repo__version");
        versionElem.append(versionText);

        // Add the version to the meta
        metaElem.appendChild(versionElem);

        // Add the time
        const timeElem = document.createElement("time");
        const timeText = document.createTextNode(time);
        timeElem.classList.add("repo__time");
        timeElem.append(timeText);

        // Add the time to the meta
        metaElem.appendChild(timeElem);

        // Add the meta to the item
        itemElem.appendChild(metaElem);

        list.appendChild(itemElem);
      });

      // Show the activity
      activity.classList.remove("activity--hidden");
    })
    .catch(error => console.log(error));
}

function handleResponse(response) {
  let contentType = response.headers.get("content-type");
  if (contentType.includes("application/json")) {
    return handleJSONResponse(response);
  } else if (contentType.includes("text/html")) {
    return handleTextResponse(response);
  } else {
    throw new Error(`Sorry, content-type ${contentType} not supported`);
  }
}

function handleJSONResponse(response) {
  return response.json().then(json => {
    if (response.ok) {
      return json;
    } else {
      return Promise.reject(
        Object.assign({}, json, {
          status: response.status,
          statusText: response.statusText
        })
      );
    }
  });
}

function handleTextResponse(response) {
  return response.text().then(text => {
    if (response.ok) {
      return json;
    } else {
      return Promise.reject({
        status: response.status,
        statusText: response.statusText,
        err: text
      });
    }
  });
}
